require "rails_helper"

RSpec.describe Ride::MonthlyStatistic do
  let(:today) { Time.zone.today }
  let(:first_day) { today.beginning_of_month }
  let(:second_day) { first_day + 1.day }
  let(:user) { FactoryGirl.create(:user) }
  let(:uber) { FactoryGirl.create(:taxi, name: "Uber#{Time.now.to_f}", user: user) }
  let(:sawa) { FactoryGirl.create(:taxi, name: "Sawa#{Time.now.to_f}", user: user) }
  let(:ride) { FactoryGirl.create(:ride, user: user, price: 3.75, date: first_day, taxi: uber) }
  let(:second_ride) { FactoryGirl.create(:ride, user: user, price: 2.22, date: first_day, taxi: sawa) }
  let(:third_ride) { FactoryGirl.create(:ride, user: user, price: 1.15, date: second_day) }
  let(:forth_ride) { FactoryGirl.create(:ride, user: user, price: 2.33, date: first_day) }
  let(:another_ride) { FactoryGirl.create(:ride, user: user, price: 1.45, date: first_day - 1.day) }

  let(:collection) { Ride::MonthlyStatistic.call(user: user).collection }

  before do
    geocoder_body = File.read(Rails.root.join("spec", "fixtures", "geocoder.json"))
    distance_matrix_body = File.read(Rails.root.join("spec", "fixtures", "distance_matrix.json"))

    url = "http://maps.googleapis.com/maps/api/geocode/json"\
      "?address=Szafranowa%204D/4,%20Gdynia,%20Poland&language=en&sensor=false"
    stub_request(:get, url).to_return(status: 200, body: geocoder_body)

    url = "http://maps.googleapis.com/maps/api/geocode/json"\
      "?address=Jelenia%2033,%20Gdynia,%20Poland&language=en&sensor=false"
    stub_request(:get, url).to_return(status: 200, body: geocoder_body)

    url = "https://maps.googleapis.com/maps/api/distancematrix/json"\
      "?destinations=40.75035,-73.99337&key=#{ENV["GOOGLE_API_KEY"]}&origins=40.75035,-73.99337"
    stub_request(:get, url).to_return(status: 200, body: distance_matrix_body)
  end

  context "consider only ride from current month" do
    before do
      ride
      second_ride
      third_ride
      another_ride
    end

    it "returns sorted dates from current month" do
      dates = collection.map(&:date)

      expect(dates.size).to eql(2)
      expect(dates).not_to eql(another_ride.date)
      expect(dates[0]).to eql(ride.date)
      expect(dates[1]).to eql(third_ride.date)
    end
  end

  context "total and average values" do
    before do
      ride
      second_ride
      forth_ride
    end

    it "returns total distance" do
      distance = collection.first.total_distance
      expect(distance).to eql(ride.distance + second_ride.distance + forth_ride.distance)
    end

    it "returns average distance" do
      distance = collection.first.average_distance
      expect(distance).to eql((ride.distance + second_ride.distance + forth_ride.distance) / 3)
    end

    it "returns total price" do
      price = collection.first.total_price
      expect(price).to eql(ride.price + second_ride.price + forth_ride.price)
    end

    it "returns average price" do
      price = collection.first.average_price
      expect(price.round(2)).to eql(((ride.price + second_ride.price + forth_ride.price) / 3).round(2))
    end
  end

  context "with different taxis" do
    before do
      ride
      second_ride
    end

    it "returns Uber and Sawa" do
      taxis = collection.first.taxis.split(", ")
      expect(taxis).to include(ride.taxi.name)
      expect(taxis).to include(second_ride.taxi.name)
    end
  end
end

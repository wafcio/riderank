require "rails_helper"

RSpec.describe User::FindOrCreateFromOmniauth do
  context "with facebook provider" do
    let(:provider) { "facebook" }
    let(:provider_field) { "facebook_uid" }

    include_examples "find or create user from omniauth"
  end

  context "with google provider" do
    let(:provider) { "google_oauth2" }
    let(:provider_field) { "google_uid" }

    include_examples "find or create user from omniauth"
  end

  context "with github provider" do
    let(:provider) { "github" }
    let(:provider_field) { "github_uid" }

    include_examples "find or create user from omniauth"
  end

  context "with not supported provider" do
    let(:auth) { OmniAuth::AuthHash.new(auth_hash) }
    let(:auth_hash) do
      {
        provider: provider,
        uid:      "12345",
        name:     "John Doe",
        email:    "john.doe#{Time.now.to_f}@example.com"
      }
    end
    let(:provider) { "sample" }

    it "raise error" do
      expect { User::FindOrCreateFromOmniauth.call(auth: auth) }.to raise_error(OmniauthProviderNotSupported)
    end
  end
end

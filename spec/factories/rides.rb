FactoryGirl.define do
  factory :ride do
    date Time.zone.today
    start_address "Szafranowa 4D/4, Gdynia, Poland"
    destination_address "Jelenia 33, Gdynia, Poland"
    price 10
    taxi
  end
end

FactoryGirl.define do
  factory :taxi do
    sequence :name do |n|
      "Sample name #{n}"
    end
    user
  end
end

RSpec.shared_examples "find or create user from omniauth" do
  let(:auth) { OmniAuth::AuthHash.new(auth_hash) }
  let(:auth_hash) do
    {
      provider: provider,
      uid:      "12345",
      info:     {
        name:  "John Doe",
        email: "john.doe#{Time.now.to_f}@example.com"
      }
    }
  end

  context "when user doesn't exist" do
    it "creates new user" do
      expect(User.count).to eql(0)

      user = User::FindOrCreateFromOmniauth.call(auth: auth).user

      expect(User.count).to eql(1)
      expect(user.email).to eql(auth.info.email)
      expect(user.name).to eql(auth.info.name)
      expect(user.attributes[provider_field]).to eql(auth.uid)
    end
  end

  context "when user with email exists" do
    let(:user) { FactoryGirl.create(:user, email: auth.info.email) }

    before { user }

    it "update user with provider field" do
      expect(User.count).to eql(1)
      expect(user.attributes[provider_field]).to be_nil

      user = User::FindOrCreateFromOmniauth.call(auth: auth).user

      expect(User.count).to eql(1)
      expect(user.email).to eql(auth.info.email)
      expect(user.name).to eql(auth.info.name)
      expect(user.attributes[provider_field]).to eql(auth.uid)
    end
  end

  context "when user with provider field exists" do
    let(:user) { FactoryGirl.create(:user, provider_field => auth.uid) }

    before { user }

    it "update user" do
      expect(User.count).to eql(1)
      expect(user.attributes[provider_field]).to eql(auth.uid)

      object = User::FindOrCreateFromOmniauth.call(auth: auth).user

      expect(User.count).to eql(1)
      expect(object.email).to eql(user.email)
      expect(object.name).to eql(auth.info.name)
      expect(object.attributes[provider_field]).to eql(auth.uid)
    end
  end
end

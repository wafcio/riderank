require "rails_helper"

RSpec.describe Ride do
  let(:user) { FactoryGirl.create(:user) }
  let(:another_user) { FactoryGirl.create(:user) }

  describe "validations" do
    it "valid with unique name" do
      taxi = Taxi.new(name: "Uber", user: user)

      expect(taxi).to be_valid
    end

    it "invalid with existed name" do
      FactoryGirl.create(:taxi, user: user, name: "Uber")
      taxi = Taxi.new(name: "Uber", user: user)

      expect(taxi).not_to be_valid
    end

    it "valid with the same name but with different user" do
      FactoryGirl.create(:taxi, user: user, name: "Uber")
      taxi = Taxi.new(name: "Uber", user: another_user)

      expect(taxi).to be_valid
    end

    it "invalid with the same name which have global taxis" do
      FactoryGirl.create(:taxi, name: "Uber", user: nil)
      taxi = Taxi.new(name: "Uber", user: user)

      expect(taxi).not_to be_valid
    end
  end
end

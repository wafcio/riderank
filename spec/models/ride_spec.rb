require "rails_helper"

RSpec.describe Ride do
  describe "validations" do
    before do
      geocoder_no_results_body = File.read(Rails.root.join("spec", "fixtures", "geocoder_no_results.json"))
      url = "http://maps.googleapis.com/maps/api/geocode/json"\
          "?address=Sample%20address&language=en&sensor=false"
      stub_request(:get, url).to_return(status: 200, body: geocoder_no_results_body)

      geocoder_body = File.read(Rails.root.join("spec", "fixtures", "geocoder.json"))
      url = "http://maps.googleapis.com/maps/api/geocode/json"\
          "?address=Hutnicza%2025,%20Gdynia,%20Poland&language=en&sensor=false"
      stub_request(:get, url).to_return(status: 200, body: geocoder_body)
      url = "http://maps.googleapis.com/maps/api/geocode/json"\
          "?address=W%C5%82adys%C5%82awa%20IV%2024,%20Gdynia,%20Poland&language=en&sensor=false"
      stub_request(:get, url).to_return(status: 200, body: geocoder_body)
    end

    describe "start address attribute" do
      it "invalid with nil value" do
        ride = Ride.create
        expect(ride.errors["start_address"]).not_to be_empty
      end

      it "invalid with no existed address" do
        ride = Ride.create(start_address: "Sample address")
        expect(ride.errors["start_address"]).not_to be_empty
      end

      it "valid with existed address" do
        ride = Ride.create(start_address: "Hutnicza 25, Gdynia, Poland")
        expect(ride.errors["start_address"]).to be_empty
        expect(ride.start_lat).not_to be_nil
        expect(ride.start_lng).not_to be_nil
      end
    end

    describe "destination address attribute" do
      it "invalid with nil value" do
        ride = Ride.create
        expect(ride.errors["destination_address"]).not_to be_empty
      end

      it "invalid with no existed address" do
        ride = Ride.create(destination_address: "Sample address")
        expect(ride.errors["destination_address"]).not_to be_empty
      end

      it "valid with existed address" do
        ride = Ride.create(destination_address: "Władysława IV 24, Gdynia, Poland")
        expect(ride.errors["destination_address"]).to be_empty
        expect(ride.destination_lat).not_to be_nil
        expect(ride.destination_lng).not_to be_nil
      end
    end

    describe "distance attribute" do
      context "without addresses" do
        let(:ride) { Ride.create }

        it "leaves distance empty" do
          expect(ride.distance).to be_nil
        end
      end

      context "with start_address" do
        let(:ride) { Ride.create(start_address: "Hutnicza 25, Gdynia, Poland") }

        it "leaves distance empty" do
          expect(ride.distance).to be_nil
        end
      end

      context "with destination_address" do
        let(:ride) { Ride.create(destination_address: "Władysława IV 24, Gdynia, Poland") }

        it "leaves distance empty" do
          expect(ride.distance).to be_nil
        end
      end

      context "with start and destination address" do
        let(:ride) do
          Ride.create(
            start_address:       "Hutnicza 25, Gdynia, Poland",
            destination_address: "Władysława IV 24, Gdynia, Poland"
          )
        end

        before do
          distance_matrix_body = File.read(Rails.root.join("spec", "fixtures", "distance_matrix.json"))
          url = "https://maps.googleapis.com/maps/api/distancematrix/json"\
            "?destinations=40.75035,-73.99337&key=#{ENV["GOOGLE_API_KEY"]}&origins=40.75035,-73.99337"
          stub_request(:get, url).to_return(status: 200, body: distance_matrix_body)
        end

        it "fills distance empty" do
          expect(ride.distance).not_to be_nil
        end
      end
    end
  end
end

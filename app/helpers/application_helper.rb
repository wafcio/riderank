module ApplicationHelper
  def distance_in_km(value)
    value.to_i / 1_000
  end
end

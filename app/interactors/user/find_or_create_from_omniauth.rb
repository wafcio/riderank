class User::FindOrCreateFromOmniauth
  include Interactor

  delegate :auth, to: :context

  def call
    provider_field = get_provider_field_name(auth.provider)
    user = find_by_uid(provider_field) || find_by_email || build
    update_user(user, provider_field)
    context.user = user
  end

  private

  def find_by_uid(provider_field)
    User.find_by(provider_field => auth.uid)
  end

  def find_by_email
    User.find_by(email: auth.info.email)
  end

  def build
    User.new
  end

  def update_user(user, provider_field)
    new_attributes = { email: auth.info.email, name: auth.info.name, provider_field => auth.uid }
    user_attributes = user.attributes.compact
    user.update!(new_attributes.merge(user_attributes))
  end

  def get_provider_field_name(provider)
    provider_field = User::GetProviderFieldName.call(provider: provider).field
    provider_field || raise(OmniauthProviderNotSupported)
  end
end

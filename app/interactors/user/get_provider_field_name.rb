class User::GetProviderFieldName
  include Interactor

  delegate :provider, to: :context

  def call
    context.field = data[provider.to_sym]
  end

  private

  def data
    {
      facebook:      :facebook_uid,
      github:        :github_uid,
      google_oauth2: :google_uid
    }
  end
end

class Ride::CalculateDistance
  include Interactor

  delegate :start_lat, :start_lng, :destination_lat, :destination_lng, to: :context

  def call
    matrix = GoogleDistanceMatrix::Matrix.new
    matrix.origins << start_place
    matrix.destinations << destination_place
    result = matrix.data[0][0]
    context.distance = result.status == "ok" ? result.distance_in_meters : 0
  end

  private

  def start_place
    GoogleDistanceMatrix::Place.new(lat: start_lat, lng: start_lng)
  end

  def destination_place
    GoogleDistanceMatrix::Place.new(lat: destination_lat, lng: destination_lng)
  end
end

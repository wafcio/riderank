class Ride::MonthlyStatistic < Ride::BaseStatistic
  include Interactor

  def call
    sql = command(start_date, end_date).to_sql
    collection = execute(sql)
    context.collection = mapping(collection)
  end

  private

  def mapping(collection)
    collection.map { |element| RideStat.new(element) }
  end

  def command(start_date, end_date)
    super.group("date").order("date")
  end

  def start_date
    Time.zone.today.beginning_of_month
  end

  def end_date
    Time.zone.today.end_of_month
  end

  def select_fields
    super << rides_table[:date]
  end
end

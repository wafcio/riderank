class Ride::WeeklyStatistic < Ride::BaseStatistic
  include Interactor

  def call
    sql = command(start_date, end_date).to_sql
    collection = execute(sql)
    context.statistic = mapping(collection).first
  end

  private

  def mapping(collection)
    collection.map { |element| RideStat.new(element) }
  end

  def start_date
    Time.zone.today.beginning_of_week
  end

  def end_date
    Time.zone.today.end_of_week
  end
end

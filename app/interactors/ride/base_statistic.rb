class Ride::BaseStatistic
  private

  def command(start_date, end_date)
    start_date_db = start_date.to_s(:db)
    end_date_db = end_date.to_s(:db)

    rides_table.
      project(*select_fields).
      where(rides_table[:date].gteq(start_date_db).and(rides_table[:date].lteq(end_date_db))).
      join(taxis_table).on(taxis_table[:id].eq(rides_table[:taxi_id]))
  end

  def execute(sql)
    ActiveRecord::Base.connection.execute(sql).to_a
  end

  def rides_table
    Ride.arel_table
  end

  def taxis_table
    Taxi.arel_table
  end

  def select_fields
    [
      rides_table[:distance].sum.as("total_distance"),
      rides_table[:distance].average.as("average_distance"),
      rides_table[:price].sum.as("total_price"),
      rides_table[:price].average.as("average_price"),
      "ARRAY_TO_STRING(ARRAY_AGG(DISTINCT taxis.name), ', ') AS taxis",
    ]
  end
end

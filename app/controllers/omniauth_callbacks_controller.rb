class OmniauthCallbacksController < ApplicationController
  skip_before_action :authenticate_user!

  def callback
    interactor = User::FindOrCreateFromOmniauth.call(auth: request.env["omniauth.auth"])
    session[:user_id] = interactor.user.id
    redirect_to root_url
  end
end

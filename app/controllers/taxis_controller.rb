class TaxisController < ApplicationController
  def index
    global_taxis = Taxi.without_user.sorted
    taxis = current_user.taxis.sorted
    render locals: { global_taxis: global_taxis, taxis: taxis }
  end

  def new
    taxi = Taxi.new
    render locals: { taxi: taxi }
  end

  def create
    taxi = Taxi.new(taxi_params.merge(user: current_user))
    if taxi.save
      redirect_to :taxis, notice: t("taxi.created")
    else
      render :new, locals: { taxi: taxi }
    end
  end

  def edit
    taxi = current_user.taxis.find(params[:id])
    render locals: { taxi: taxi }
  end

  def update
    taxi = current_user.taxis.find(params[:id])
    if taxi.update(taxi_params)
      redirect_to :taxis, notice: t("taxi.updated")
    else
      render :new, locals: { taxi: taxi }
    end
  end

  def destroy
    taxi = current_user.taxis.find(params[:id])
    if taxi.rides.exists?
      redirect_to :taxis, notice: t("taxi.cannot_delete")
    else
      taxi.destroy
      redirect_to :taxis, notice: t("taxi.deleted")
    end
  end

  private

  def taxi_params
    params.fetch(:taxi, {}).permit(:name)
  end
end

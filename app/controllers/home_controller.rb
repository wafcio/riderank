class HomeController < ApplicationController
  def index
    monthly_statistic = Ride::MonthlyStatistic.call(user: current_user).collection
    weekly_statistic = Ride::WeeklyStatistic.call(user: current_user).statistic
    render locals: { monthly_statistic: monthly_statistic, weekly_statistic: weekly_statistic }
  end
end

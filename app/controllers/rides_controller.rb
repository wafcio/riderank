class RidesController < ApplicationController
  def index
    rides = current_user.rides.sorted.includes(:taxi)
    render locals: { rides: rides }
  end

  def new
    ride = Ride.new(date: Time.zone.today)
    render locals: { ride: ride, taxis: global_with_user_taxis }
  end

  def create
    ride = Ride.new(ride_params.merge(user: current_user))
    if ride.save
      redirect_to :rides, notice: t("ride.created")
    else
      render :new, locals: { ride: ride, taxis: global_with_user_taxis }
    end
  end

  def edit
    ride = current_user.rides.find(params[:id])
    render locals: { ride: ride, taxis: global_with_user_taxis }
  end

  def update
    ride = current_user.rides.find(params[:id])
    if ride.update(ride_params)
      redirect_to :rides, notice: t("ride.updated")
    else
      render :new, locals: { ride: ride, taxis: global_with_user_taxis }
    end
  end

  def destroy
    ride = current_user.rides.find(params[:id])
    ride.destroy
    redirect_to :rides, notice: t("ride.deleted")
  end

  private

  def ride_params
    params.fetch(:ride, {}).permit(:start_address, :destination_address, :price, :taxi_id, :date)
  end

  def global_with_user_taxis
    Taxi.with_user_ids(nil, current_user.id).sorted
  end
end

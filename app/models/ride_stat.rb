class RideStat
  include ActiveModel::Model

  attr_reader :date
  attr_accessor :total_distance, :average_distance, :total_price, :average_price, :taxis

  def date=(value)
    @date = Date.parse(value)
  end
end

class Taxi < ApplicationRecord
  scope :sorted, -> { order(:name) }
  scope :without_user, -> { where(user_id: nil) }
  scope :with_user_ids, ->(*user_ids) { where(user_id: user_ids) }

  belongs_to :user, required: false
  has_many :rides, dependent: :destroy

  validates :name, presence: true, uniqueness: { scope: :user_id }
  validate :check_name_with_global_taxi

  private

  def check_name_with_global_taxi
    errors.add(:name, :taken) if Taxi.without_user.where(name: name).exists?
  end
end

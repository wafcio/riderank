class User < ApplicationRecord
  has_many :rides
  has_many :taxis

  validates :name, :email, presence: true
end

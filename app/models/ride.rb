class Ride < ApplicationRecord
  scope :sorted, -> { order(:date, :id) }

  belongs_to :user
  belongs_to :taxi

  validates :start_address, :destination_address, :price, :taxi, presence: true
  validates :price, numericality: true
  validates :date, date: true

  before_validation :fill_start_and_destination_coordinates
  after_validation :fill_distance

  def coordinates_blank?
    start_lat.blank? || start_lng.blank? || destination_lat.blank? || destination_lng.blank?
  end

  def coordinates_changed?
    start_lat_changed? || start_lng_changed? || destination_lat_changed? || destination_lng_changed?
  end

  private

  def fill_start_and_destination_coordinates
    fill_coordinates(:start) if start_address.present?
    fill_coordinates(:destination) if destination_address.present?
  end

  def fill_coordinates(prefix)
    return unless send("#{prefix}_address_changed?")

    results = Geocoder.search(send("#{prefix}_address"))
    if results[0]
      coordinates = results[0].coordinates
      send("#{prefix}_lat=", coordinates[0])
      send("#{prefix}_lng=", coordinates[1])
    else
      errors.add("#{prefix}_address", :not_found)
    end
  end

  def fill_distance
    return if coordinates_blank?
    return unless coordinates_changed?

    self.distance = Ride::CalculateDistance.call(
      start_lat:       start_lat,
      start_lng:       start_lng,
      destination_lat: destination_lat,
      destination_lng: destination_lng
    ).distance
  end
end

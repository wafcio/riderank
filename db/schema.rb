# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170116141338) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "rides", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "start_address",       null: false
    t.string   "destination_address", null: false
    t.float    "price",               null: false
    t.float    "distance",            null: false
    t.date     "date",                null: false
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.float    "start_lat"
    t.float    "start_lng"
    t.float    "destination_lat"
    t.float    "destination_lng"
    t.integer  "taxi_id"
    t.index ["taxi_id"], name: "index_rides_on_taxi_id", using: :btree
    t.index ["user_id"], name: "index_rides_on_user_id", using: :btree
  end

  create_table "taxis", force: :cascade do |t|
    t.string   "name",       null: false
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_taxis_on_user_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "name",         null: false
    t.string   "email",        null: false
    t.string   "facebook_uid"
    t.string   "github_uid"
    t.string   "google_uid"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_foreign_key "rides", "taxis"
  add_foreign_key "rides", "users"
end

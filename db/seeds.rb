# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)

Taxi.without_user.destroy_all
uber = Taxi.where(name: "Uber").first_or_create!
Taxi.where(name: "Sawa").first_or_create!
Taxi.where(name: "MPT").first_or_create!

user = User.where(email: "krzysztof.wawer@gmail.com").first_or_create!(name: "Krzysztof Wawer")
user.rides.delete_all

user.rides.create!(
  start_address:       "Szafranowa 4D/4, Gdynia, Poland",
  destination_address: "Jelenia 33, Gdynia, Poland",
  price:               4.75,
  taxi:                uber,
  date:                Time.zone.today
)
user.rides.create!(
  start_address:       "Szafranowa 4D/4, Gdynia, Poland",
  destination_address: "Hutnicza 25, Gdynia, Poland",
  price:               3.22,
  taxi:                uber,
  date:                Time.zone.today
)
user.rides.create!(
  start_address:       "Szafranowa 4D/4, Gdynia, Poland",
  destination_address: "Władysława IV 24, Gdynia, Poland",
  price:               123_456,
  taxi:                uber,
  date:                1.day.ago
)

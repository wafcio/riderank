class RemoveTaxiProviderFromRide < ActiveRecord::Migration[5.0]
  def up
    remove_column :rides, :taxi_provider
  end

  def down
    add_column :rides, :taxi_provider, :string
  end
end

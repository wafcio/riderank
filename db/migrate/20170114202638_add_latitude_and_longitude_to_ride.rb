class AddLatitudeAndLongitudeToRide < ActiveRecord::Migration[5.0]
  def change
    add_column :rides, :start_lat, :float
    add_column :rides, :start_lng, :float
    add_column :rides, :destination_lat, :float
    add_column :rides, :destination_lng, :float
  end
end

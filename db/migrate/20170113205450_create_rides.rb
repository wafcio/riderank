class CreateRides < ActiveRecord::Migration[5.0]
  def change
    create_rides_table

    add_foreign_key :rides, :users
  end

  def create_rides_table
    create_table :rides do |t|
      t.references :user
      t.string :start_address, null: false
      t.string :destination_address, null: false
      t.float :price, null: false
      t.string :taxi_provider, null: false
      t.float :distance, null: false
      t.date :date, null: false

      t.timestamps
    end
  end
end

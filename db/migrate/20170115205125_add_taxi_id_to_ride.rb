class AddTaxiIdToRide < ActiveRecord::Migration[5.0]
  def change
    add_reference :rides, :taxi, index: true
    add_foreign_key :rides, :taxis
  end
end

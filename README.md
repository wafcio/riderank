# Riderank

## Local setup

* Clone the repo

  ```
  $ git clone git@bitbucket.org:kwawer/riderank.git
  ```

* Install Postgres

  Install [brew bundle](https://github.com/Homebrew/homebrew-bundle) as a Homebrew tap:

  ```
  $ brew tap Homebrew/bundle
  ```

  then install dependencies from a Brewfile:

  ```
  $ brew bundle
  ```

  or consult [postgresql](http://www.postgresql.org/download/).

* Set up environment variables

  Riderank uses [dotenv](https://github.com/bkeepers/dotenv) to manage environment variables. Create the `.env` file by copying the example file:

  ```
  $ cp .env.example .env
  ```

* Configure Github OAuth

  Go to [your settings](https://github.com/settings/applications/new) to register a new app and fill in form with:

  ```
  Homepage URL: http://localhost:3000/
  Authorization callback URL: http://localhost:3000/auth/github/callback
  ```

  or if you are using [pow](http://pow.cx/):

  ```
  Homepage URL: http://riderank.dev/
  Authorization callback URL: http://riderank.dev/auth/github/callback
  ```

  Register application and update `GITHUB_APP_ID` and `GITHUB_APP_SECRET` in your `.env` file.

* Configure Facebook OAuth

  Go to [your settings](https://developers.facebook.com) and create a new app first. Fill in form with:
  
  ```
  Authorized redirect URIs: http://localhost:3000/auth/facebook/callback
  ```

  or if you are using [pow](http://pow.cx/):

  ```
  Authorized redirect URIs: http://riderank.dev/auth/facebook/callback
  ```

  Register application and update `FACEBOOK_APP_ID` and `FACEBOOK_APP_SECRET` in your `.env` file.
  
* Configure Google OAuth

  Go to [your console](https://console.developers.google.com/project/) and create a new project first. Then use API Manager > Credentials > Create credentials > OAuth client ID to generate client ID and client secret. Fill in form with:

  ```
  Authorized redirect URIs: http://localhost:3000/auth/google_oauth2/callback
  ```

  or if you are using [pow](http://pow.cx/):

  ```
  Authorized redirect URIs: http://riderank.dev/auth/google_oauth2/callback
  ```

  Register application and update `GOOGLE_APP_ID` and `GOOGLE_APP_SECRET` in your `.env` file. Make sure that you have Google+ API enabled for the project on the API Manager > Library page.
  
* Configure Google Maps Distance Matrix API

  Go to [your console](https://console.developers.google.com/project/) and choose your project.  Then use API Manager > Credentials > Create credentials > API key to generate api key.
  
  Make sure that you have Google Maps Distance Matrix API enabled for the project on the Library page.

  Register application and update `GOOGLE_API_KEY` in your `.env` file.

* Set up the database

  Start postgres server and load the current schema into the database and seed it:

  ```
  $ bin/rake db:setup
  ```


* Make sure the test suite passes

  Run:

  ```
  $ bin/rspec
  ```

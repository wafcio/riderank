Rails.application.routes.draw do
  root "home#index"

  match "/auth/:provider" => "omniauth_callbacks#passthru", via: [:get, :post], as: :omniauth
  match "/auth/:provider/callback" => "omniauth_callbacks#callback", via: [:get, :post]
  get "sign_in" => "sessions#new", as: :sign_in
  get "sign_out" => "sessions#destroy", as: :sign_out

  resources :rides, except: [:show]
  resources :taxis, except: [:show]
end

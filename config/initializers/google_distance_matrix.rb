GoogleDistanceMatrix.configure_defaults do |config|
  config.google_api_key = ENV.fetch("GOOGLE_API_KEY")
end
